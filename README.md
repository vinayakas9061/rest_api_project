# Rest_Api_project


## Introduction

#AUTOMATION TESTING OF REST APIs USING POSTMAN

## FEATURES
 
 1) Here we used the different types of Rest API methods such as POST,PUT,PATCH,GET & DELETE using Postman.
 2) Automated the APIs using the Environmental variables.
 3) Automated the APIs by Data Driven Testing method using Json file and Excel data file.
 4) Tested the APIs by using the Dynamic variables.
 5) Used the concept of Request chaining for one of the POST API. 
 6) Generated the Reports using Newman Tool with the HTML Report & HTML Report extraa method.

 ## Source of API

 #https://reqres.in/
